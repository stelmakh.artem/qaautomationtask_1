Run tests:
To run the test from Maven need to use the command mvn clean install

Create new test
1. Open already existing file .feature
    - Start to Create from step add Scenario with the name for this Scenario
    - Below start to  write steps used for it keywords 'Give', 'When', 'And' , 'Then'. All steps which will created should be implemented in step class

2. Create new .feature file
    - add new Feature: "Name about this feature" other steps like before

Refactor:
1. Open project as Maven for import maven libraries
2. Deleted all files according to gradle because we use Maven
3. Removed empty folder History
4. Remove build folder because it does not contain any useful information
5. Deleted distributionManagement from pom file because we don't have any local library maven and also .m2 folder with configure
6. Add Lombok,jackson and serenity-model  libraries for reproducing code in classes and extending functionality
7. Excluded parallel settings run cases from pom at now we have only several cases , when it's needed  we will have a lot of cases we can add it
8. Add environments global URL in serenity.conf file for able run cases on different env by -Denvironment=nameEnv
9. Add serenity report config in serenity.conf for generate report
10. Updated includes for able run all class with value which add for include
11. Rename post_product.feature to get_product.feature because all cases in this feature work on get info
12. Feature: Search for the product, was separated on several cases because in the first verified  the correct response with the correct url parameter
    in the second case, we verified status code 404 with an incorrect parameter. Before we have one Scenario with all steps. One case should verify one point.
    Also add params for url parameter for use in the same steps.
13. Separated logic  between class in SearchProductsAPI work with methods for work with some api and SearchProductStep with steps for processing data, verify, etc.
14. Created HeaderConfigs class where we can prepare headers for request
15. Created HTTPMethods Wrapper class for all possible actions API which we can use in our request
16. Created Config class in this class we set and get some params like ENV
17. Created APIPaths class from here we can get  path for basePath in request
18. Create Model class for Product items for work with response field
19. Created interface where to describe the methods of how you can work with the class
20. Create class for implement methods from interface
21. Separated files between folders for data structuring