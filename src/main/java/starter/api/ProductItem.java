package starter.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductItem{
	private String image;
	private String unit;
	private String provider;
	private Object price;
	private String title;
	private String promoDetails;
	private String brand;
	@JsonProperty("isPromo")
	private boolean isPromo;
	private String url;
}