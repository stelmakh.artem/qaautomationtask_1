package starter.config;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class Config {

    static EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    public static String ENV =  EnvironmentSpecificConfiguration.from(variables)
            .getProperty("api.url");
}
