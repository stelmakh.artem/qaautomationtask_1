package starter.helper;

import io.restassured.response.ResponseBody;
import net.serenitybdd.rest.SerenityRest;
import starter.config.Config;


/**
 * Wrapper class for all possible actions API
 */
public class HTTPMethods {
    static HeaderConfigs header = new HeaderConfigs();

    public static ResponseBody get(String urlPath) {
        ResponseBody response = SerenityRest.given().
                headers(header.defaultHeaders())
                .baseUri(Config.ENV).
                        basePath(urlPath)
                .get().getBody();
        return response;
    }



}
