package starter.helper;

import io.restassured.http.ContentType;

import java.util.HashMap;
import java.util.Map;

public class HeaderConfigs {

     public static Map<String, String> defaultHeaders(){
        Map<String, String> defaultHeaders = new HashMap<String, String>();
        defaultHeaders.put("Content-Type", String.valueOf(ContentType.JSON));
        return defaultHeaders;
    }
}
