package starter.impl;

import starter.api.ProductItem;
import starter.service.ProductItemService;

import static net.serenitybdd.rest.SerenityRest.*;

import java.util.ArrayList;
import java.util.List;

public class ProductItemImpl implements ProductItemService {

    @Override
    public List<String> getTitles() {
        List<ProductItem> products = then().extract().jsonPath().getList(".", ProductItem.class);
        List<String> titles = new ArrayList<>();
        for (ProductItem product: products) {
            titles.add(product.getTitle());
        }
        return titles;

    }


}
