package starter.service;

import java.util.List;

public interface ProductItemService {

    List<String> getTitles();

}
