package starter.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.impl.ProductItemImpl;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.equalTo;

public class SearchProductStep {

    @Steps
    public SearchProductsAPI carsAPI;

    @Given("he calls endpoint {string}")
    public void heCallsEndpoint(String itemName) {
        carsAPI.searchProductByItem(itemName);
    }


    @Then("he sees the results displayed for {string}")
    public void userSeesTheResultsDisplayedForSearch(String itemName) {
        ProductItemImpl productItem = new ProductItemImpl();
        List<String> getTitles =  productItem.getTitles();
        restAssuredThat(response -> response.statusCode(200));
        getTitles.forEach(title -> {
            Assert.assertTrue(String.format("In Search Result '%s' searched item %s is not present ",title, itemName),  title.toLowerCase().contains(itemName));
        });

    }

    @Then("he doesn't see the results")
    public void userDoesNotSeeTheResultsForIncorrectSearch() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

}
