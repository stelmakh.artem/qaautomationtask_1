package starter.steps;

import net.thucydides.core.annotations.Step;
import starter.config.APIPaths;
import starter.helper.HTTPMethods;

public class SearchProductsAPI {

    @Step("Get search product response by item {0}")
    public void searchProductByItem(String items) {
        HTTPMethods.get(APIPaths.searchProduct + items);
    }
}
