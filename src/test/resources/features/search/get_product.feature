Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios
#
  Scenario Outline: Verify the search product works
    Given he calls endpoint "<items>"
    Then he sees the results displayed for "<items>"
    Examples:
      | items |
      | cola |
      | orange |
      | apple |
      | pasta |

  Scenario:Verify the search product get error with incorrect data
    Given he calls endpoint "cars"
    Then he doesn't see the results
